#include "rad_def.inc"
 
global_settings {
  //This setting is for alpha transparency to work properly.
  //Increase by a small amount if transparent areas appear dark.
   max_trace_level 15
   assumed_gamma 1
 
   radiosity{Rad_Settings(Radiosity_Normal, off, off)}
}

#include "macro.inc"
#include "pelvis_geom.inc" 
#include "bladder_geom.inc" 
 
//CAMERA PoseRayCAMERA
camera {
   perspective
   up y
   right -x*image_width/image_height
   location <0,850,0>
   angle 26.15064 // horizontal FOV angle
   rotate  <-5,0,0>
   look_at <0,0,0>
}
 
//PoseRay default Light attached to the camera
light_source {
   <0,850,0> //light position
   color rgb <1,1,1>*1.6
   parallel
   rotate  <-5,0,0>
   point_at <0,0,0>
}
 
//Background
background { color srgb<1,1,1>  }
 
//Assembled object that is contained in pelvis_geom.inc with no SSLT components
object{ 
   pelvis_ 
   translate<12,100,0> 
}

object{ 
   bladder_ 
   translate<12,100,0> 
}

object{ 
   Raster_Plate(
      20.0, 0.02,// RD1, Width1,// 1st distance/width,
      0, 0,// RD2, Width2,// 2nd distance/width,
      B_Tex, // Base_Texture, // non = pigment{Clear}
      0.0, // Intensity, // Line gray intensity
      <-120,-0.05,-120>, <120,0,120>, //Start/End
   ) //----------------------------------------------
   rotate<0,0,0>
   translate<0,0,0>
}

object{
   AxisXYZ(0.8,0.8,0.8) 
   scale 20
   translate<-100,0,-100>
}

//==================================================
