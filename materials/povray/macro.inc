//------------------------------------- ////////////
#macro Raster_Lines (RScale, LineW, Intensity_)
 pigment{ gradient x scale RScale
          color_map{
           [0.000   color rgbt<1,1,1,0>*Intensity_]
           [0+LineW color rgbt<1,1,1,0>*Intensity_]
           [0+LineW color rgbt<1,1,1,1>]
           [1-LineW color rgbt<1,1,1,1>]
           [1-LineW color rgbt<1,1,1,0>*Intensity_]
           [1.000   color rgbt<1,1,1,0>*Intensity_]
           } // end color_map
         } // end pigment
#end
//------------------------------------- ////////////

#macro Raster_Plate(
    RD1, Width1,//primary raster distance/width,
    RD2, Width2,//secondary raster distance/width,
    Base_Texture, //  non = pigment { Clear }
    Intensity, // Line gray intensity
    Start_Box, End_Box, // vectors
  ) 

box{ Start_Box,End_Box
     texture{ Base_Texture }
  #if (RD1 > 0 ) // raster big  RD1, Width1,
    texture { Raster_Lines(RD1,Width1,Intensity) }
    texture { Raster_Lines(RD1,Width1,Intensity)
              rotate<0,90,0>}
  #end
  #if (RD2 > 0 ) // raster small RD2, Width2,
    texture { Raster_Lines(RD2,Width2,Intensity) }
    texture { Raster_Lines(RD2,Width2,Intensity)
              rotate<0,90,0>}
  #end
} 
#end 

#declare B_Tex = texture{ pigment{ color rgb<1,1,1>*1.2 }} 

//------------------------------ the Axes --------------------------------
//------------------------------------------------------------------------
#macro Axis_( AxisLen, Texture)
 union{
    cylinder { 
       <0,0,0>,<0,AxisLen,0>,0.04
       texture{
          Texture translate<0.1,0,0.1>
       }
    }

    cone{
       <0,AxisLen,0>,0.1,<0,AxisLen+0.2,0>,0
       texture{Texture}
    }
 } // end of union
#end // of macro "Axis()"
//------------------------------------------------------------------------

#macro AxisXYZ( AxisLenX, AxisLenY, AxisLenZ)

union{

#if (AxisLenX != 0)

#declare Tex_Red  = texture {
   pigment{ color rgb<1,0,0>}
//   finish { phong 1}
}

object{ 
   Axis_(AxisLenX, Tex_Red)   
   rotate< 0,0,-90>
}// x-Axis

text{ 
   ttf "arial.ttf",  "x",  0.15,  0  texture{Tex_Red}
   rotate <90,0,0> scale 0.5 translate <AxisLenX+0.4,0.1,-0.1> no_shadow
}

#end // of #if

#if (AxisLenY != 0)

#declare Tex_Green  = texture{
   pigment{ color rgb<0,1,0>}
//   finish { phong 1}
}

object{ 
   Axis_(AxisLenY, Tex_Green)   
   rotate< 0,0,  0>
}// y-Axis

text{ 
   ttf "arial.ttf",  "y",  0.15,  0  texture{Tex_Green}
   rotate <90,180,180> scale 0.5 translate <-0.4,AxisLenY+0.40,-0.05> no_shadow }
#end // of #if

#if (AxisLenZ != 0)

#declare Tex_Blue  = texture{
   pigment{ color rgb<0,0,1>}
//  finish { phong 1}
}

object{ 
   Axis_(AxisLenZ, Tex_Blue)  
   rotate<90,0,0>
}// z-Axis

text{ 
   ttf "arial.ttf",  "z",  0.15,  0  texture{Tex_Blue}
   rotate <90,0,180> scale 0.5 translate <0,0.1,AxisLenZ+0.4> no_shadow }
#end // of #if

} // end of union

#end// of macro "AxisXYZ( ... )"

//-------------------------------------------------- end of coordinate axes

