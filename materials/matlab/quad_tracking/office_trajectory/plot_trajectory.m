clear all;
close all;

load('gt_cam');
load('cam.mat');

plot3(gt_cam(1,:),gt_cam(2,:),gt_cam(3,:),'r-o');
grid on;
hold on;

plot3(cam(1,:),cam(2,:),cam(3,:),'b-+');

view(-66, 12);
legend('Ground truth', 'Quadrifocal tracking', 'Location', 'NorthEast');
xlabel('X');
ylabel('Y');
zlabel('Z');

rms = sqrt(sum((cam(:) - gt_cam(:)).^2/size(gt_cam,2)))