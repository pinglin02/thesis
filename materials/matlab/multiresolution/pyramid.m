clear all;
close all;

I_pyr = cell(6);

I_pyr{1} = imread('1_level.png');

img_size = size(I_pyr{1});

num_level_pyr = 6;

for l=2:num_level_pyr
    I_pyr{l} = impyramid(I_pyr{l-1},'reduce');    
    img_out = imresize(I_pyr{l}, 2^(l-1), 'nearest');
    
    file_name = sprintf('%d_level.png', l);
    imwrite(img_out,file_name);
end

