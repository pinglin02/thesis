clear all;
close all;

patch_size = 80;

patch = zeros(patch_size,patch_size);

band_start = floor(patch_size*2.4/5);
band_end = floor(patch_size*2.6/5);

max_intensity = 0.8;

patch(:, 1:band_start) = max_intensity;
patch(:, (band_end+1):patch_size) = 0;

for i = (band_start+1):band_end  
    patch(:, i) = max_intensity - (max_intensity/(band_end-band_start+1))*(i-band_start);           
end

diag_patch = imrotate(patch, 45, 'bilinear', 'crop');

%crop
diag_patch = diag_patch(patch_size*2/10:patch_size*8/10, ...
                        patch_size*2/10:patch_size*8/10);

% noise_patch = imnoise(patch, 'gaussian', 0, 0.003);
noise_patch = imnoise(diag_patch, 'gaussian', 0, 0.01);

[xg, yg] = meshgrid(0:size(noise_patch, 1)-1, 0:size(noise_patch, 2)-1);

w_size= 20;

% Original image
figure('name', 'Ori Image');
imshow(diag_patch','Border','tight','InitialMagnification',100);
print('imgs/ori_img.eps','-depsc','-r300'); 

figure('name', 'Ori Image Mesh');
surf(xg, yg, diag_patch,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong');
colormap(bone);

caxis([-0.1 1.1]);
xlim([1 size(diag_patch,1)]);
ylim([1 size(diag_patch,2)]);
zlim([0 1.0]);
xlabel('x', 'FontSize', 14);
ylabel('y', 'FontSize', 14); 
zlabel('Intensity', 'FontSize', 14); 
set(gca,'FontSize',14); 
set(gcf,'Renderer','opengl');
axis tight;
view([135 30]) ;;
print('imgs/ori_img_mesh.eps','-depsc','-r300'); 

% Noise image
figure('name', 'Noise Image');
imshow(noise_patch','Border','tight','InitialMagnification',100);
print('imgs/noise_img.eps','-depsc','-r300');

figure('name', 'Noise Image Mesh');
surf(xg, yg, noise_patch,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong');
colormap(bone);
caxis([-0.1 1.1]);
xlim([1 size(diag_patch,1)]);
ylim([1 size(diag_patch,2)]);
zlim([0 1.0]);
xlabel('x', 'FontSize', 14);
ylabel('y', 'FontSize', 14); 
zlabel('Intensity', 'FontSize', 14); 
set(gca,'FontSize',14); 
set(gcf,'Renderer','opengl');
axis tight;
view([135 30]) ;
print('imgs/noise_img_mesh.eps','-depsc','-r300');

% Box filter
h = fspecial('average', w_size);
box_patch = imfilter(noise_patch, h, 'replicate');

figure('name', 'Box Filtered Image');
imshow(box_patch','Border','tight','InitialMagnification',100);
print('imgs/box_img.eps','-depsc','-r300');

figure('name', 'Box Filtered Image Mesh');
surf(xg, yg, box_patch,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong');
colormap(bone);
caxis([-0.1 1.1]);
xlim([1 size(diag_patch,1)]);
ylim([1 size(diag_patch,2)]);
xlabel('x', 'FontSize', 14);
ylabel('y', 'FontSize', 14); 
zlabel('Intensity', 'FontSize', 14); 
set(gca,'FontSize',14);
set(gcf,'Renderer','opengl');
axis tight;
view([135 30]) ;
print('imgs/box_img_mesh.eps','-depsc','-r300');

% Gaussian filter
h = fspecial('average', w_size);
box_patch = imfilter(noise_patch, h, 'replicate');
box_patch = imfilter(box_patch, h, 'replicate');
Gaussian_patch = imfilter(box_patch, h, 'replicate');

figure('name', 'Gaussian Filtered Image');
imshow(Gaussian_patch','Border','tight','InitialMagnification',100);
print('imgs/gauss_img.eps','-depsc','-r300');

figure('name', 'Gaussian Filtered Image Mesh');
surf(xg, yg, Gaussian_patch,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong');
colormap(bone);
caxis([-0.1 1.1]);
xlim([1 size(diag_patch,1)]);
ylim([1 size(diag_patch,2)]);
xlabel('x', 'FontSize', 14);
ylabel('y', 'FontSize', 14); 
zlabel('Intensity', 'FontSize', 14); 
set(gca,'FontSize',14);
set(gcf,'Renderer','opengl');
axis tight;
view([135 30]) ;
print('imgs/gauss_img_mesh.eps','-depsc','-r300');

% Bilateral filter
sigma = [3, 0.4];
bfilter_patch = jbfilter2(noise_patch, diag_patch, floor(w_size/2), sigma);

figure('name', 'Bilater Filtered Image');
imshow(bfilter_patch','Border','tight','InitialMagnification',100);
print('imgs/bila_img.eps','-depsc','-r300');

figure('name', 'Bilater Filteried Image Mesh');
surf(xg, yg, bfilter_patch,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong');
colormap(bone); 
caxis([-0.1 1.1]);
xlim([1 size(diag_patch,1)]);
ylim([1 size(diag_patch,2)]);
xlabel('x', 'FontSize', 14);
ylabel('y', 'FontSize', 14); 
zlabel('Intensity', 'FontSize', 14); 
set(gca,'FontSize',14);
set(gcf,'Renderer','opengl');
axis tight;
view([135 30]) ;
print('imgs/bila_img_mesh.eps','-depsc','-r300');

% Guided image filter
guided_patch = guidedfilter(diag_patch, noise_patch, floor(w_size/2), 0.01);

figure('name', 'Guided Image Filtered Image');
imshow(guided_patch','Border','tight','InitialMagnification',100);
print('imgs/gi_img.eps','-depsc','-r300');

figure('name', 'Guided Image Filteried Image Mesh');
surf(xg, yg, guided_patch,'FaceColor','interp',...
   'EdgeColor','none',...
   'FaceLighting','phong');
colormap(bone); 
caxis([-0.1 1.1]);
xlim([1 size(diag_patch,1)]);
ylim([1 size(diag_patch,2)]);
xlabel('x', 'FontSize', 14);
ylabel('y', 'FontSize', 14); 
zlabel('Intensity', 'FontSize', 14); 
set(gca,'FontSize',14); 
set(gcf,'Renderer','opengl');
axis tight;
view([135 30]) ;
print('imgs/gi_img_mesh.eps','-depsc','-r300');
