clear all;
close all;

x = [-1:0.0001:1]';

y = x.*x;
figure;
plot(x, y);
title('L2 norm');
grid on;


y = abs(x);
figure;
plot(x, y);
title('L1 norm');
grid on;

L2_idx = find(x >= -0.2 & x <= 0.2);
L1_idx = find(~(x >= -0.2 & x <= 0.2));

y(L2_idx) = x(L2_idx).*x(L2_idx)/(2*0.2);
y(L1_idx) = abs(x(L1_idx)) - (0.2/2);

figure;
plot(x, y);
title('Huber function');
grid on;