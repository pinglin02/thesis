clear all;
close all;

x = (-1:0.001:1)';

%%% Cost function
figure;

% L2
y = 0.5*x.*x;
plot(x, y, 'r', 'LineWidth', 1.2);
hold on;

% L1
y = abs(x);
plot(x, y, 'g', 'LineWidth', 1.2);

% Geman-McClure
sigma = 0.5;
y = (x.*x/2)./(sigma+x.*x);
plot(x, y, 'b', 'LineWidth', 1.2);

% Tukey
c = 0.5;
outlier_idx = find(~(x >= -c & x <= c));
inlier_idx = find(x >= -c & x <= c);

y(inlier_idx) = c.^2*(1-(1-(x(inlier_idx)/c).^2).^3)/6;
y(outlier_idx) = c.^2/6;
plot(x, y, 'c', 'LineWidth', 1.2);

grid on;
h = legend('L^2', 'L^1', 'Geman-McClure', 'Tukey', 'Location', 'North');
set(h, 'FontSize', 16);
ylim([0 0.5]);

%%% Influence function
figure;

% L2
y = x;
plot(x, y, 'r', 'LineWidth', 1.2);
hold on;

% L1
y = sign(x);
plot(x, y, 'g', 'LineWidth', 1.2);

% Geman-McClure
y = x./((sigma+x.*x).*(sigma+x.*x));
plot(x, y, 'b', 'LineWidth', 1.2);
 
% Tukey
y(inlier_idx) = x(inlier_idx).*(1-(x(inlier_idx)/c).^2).^2;
y(outlier_idx) = 0;
plot(x, y, 'c', 'LineWidth', 1.2);

grid on;
h = legend('L^2', 'L^1', 'Geman-McClure', 'Tukey', 'Location', 'Northwest');
set(h, 'FontSize', 16);
ylim([-1.5 1.5]);

%%% Weight function
figure;

% L2
y = ones(size(x,1),1);
plot(x, y, 'r', 'LineWidth', 1.2);
hold on;

% L1
y = 1./abs(x);
plot(x, y, 'g', 'LineWidth', 1.2);

% Geman-McClure
y = 1./((sigma+x.*x).*(sigma+x.*x));
plot(x, y, 'b', 'LineWidth', 1.2);

% Tukey
y(inlier_idx) = (1-(x(inlier_idx)/c).^2).^2;
y(outlier_idx) = 0;
plot(x, y, 'c', 'LineWidth', 1.2);

grid on;
h = legend('L^2', 'L^1', 'Geman-McClure', 'Tukey', 'Location', 'Northwest');
set(h, 'FontSize', 16);
ylim([0 5]);
