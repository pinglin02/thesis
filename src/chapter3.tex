%\begingroup
\chapter{Mapping and Tracking Using Preoperative Models}
\label{ch:mapping_and_tracking_using_preoperative_models}
%\renewcommand{\baselinestretch}{1}
\minitoc
%\endgroup

\newpage

\section{Introduction}
The first approach we took to pure vision-based image-guided surgery was to exploit the preoperative \gls{CT}/\gls{MRI} scanned models. The idea is that preoperative models are not only of interest for the surgeons to perceive the location of underlying anatomical structures in the endoscopic images, but also that the geometric shape of anatomy from the scans can provide information for registration to the right pose. We make the assumption that the scenes are rigid so that we can perform rigid image registration to align a model with endoscopic images. In \gls{RALP}, the pelvis bone should be rigid and the pubic arch is visible in the endoscopic scene, so this is potentially useful anatomy for registration.

We propose approaches to register a non-textured rigid 3D model with a number of 2D images followed by dense monocular camera tracking using the registered model in order to continuously align the preoperative models for \gls{AR} image-guided surgery. This framework is similar to visual \gls{SLAM} but the map is now replaced with the dense preoperative model and we just have to register the dense map in the right place. Dense visual tracking has recently been shown very robust and can achieve real-time performance using \gls{GPU} computation~\citep{Newcombe2011b,Newcombe2011a}. The lack of a ground truth in 3D medical image registration has led to the suggestion of simulations for algorithm testing~\citep{Jannin2002}. We have developed a ground truth simulation to validate the performance of the proposed registration algorithm. Additionally, a fast method for calculation of the visible 3D surface points for colour-consistency calculation is proposed.

The initial idea was to use \gls{PTAM}, which is a real-time sparse feature-based monocular visual \gls{SLAM} system~\citep{Klein2007} to provide an initial set of 2D images with their camera poses, followed by photo-consistency-based multi-view 3D to 2D image registration~\citep{Clarkson2001}. The method turned out to work well with synthetic rigid scenes where the registration has a capture range of $\pm 9\mathrm{mm}/^{\circ}$ and a target registration error (TRE) less than $2$ mm. However, in real endoscopic scenes, we found that \gls{PTAM} could not provide accurate camera poses since most of the feature points were extracted from deforming regions or from specular highlights. Therefore, we abandoned \gls{PTAM} and turned to stereo vision in which we can have two rigid views to register the model.

Dense tracking using the registered and textured 3D model turns out to be extremely accurate by itself, providing that the scene is rigid and the textured model agrees with the true geometry. However, the geometric shape of the preoperative model is not exactly the same as what is observed in the real endoscopic scene, due to a thick covering of tissue over the bone and model segmentation errors. A stereo registration for a particular frame can be obtained by optimisation so that we can perspectively back-project the 2D image to texture the model. However, the dense tracking drifts rapidly in subsequent endoscopic video frames. This is because the problem is ill-conditioned due to the lack of correspondence between the model and the viewed surface. To resolve this problem, Chapter~\ref{ch:real_time_dense_stereo_reconstruction}~and~\ref{ch:real_time_sense_stereo_camera_tracking} provide algorithms for real-time stereo dense reconstruction and tracking for endoscopic scenes in a sound theoretical framework.

The method proposed in this chapter using 3D to 2D non-textured model image registration with \gls{PTAM} has been published~\citep{Chang2011,Chang2012}. The method for dense tracking using an automatically segmented preoperative model has been published in a co-authored paper~\citep{Gao2013}.

\section{Background}

A significant issue when reconstructing a 3D organ is tissue deformation. Algorithms for deformable 3D surface reconstruction can be separated into template-based and non-rigid structure from motion reconstruction. Both approaches have shown success in deformable 3D surface reconstruction~\citep{Salzmann2010}. However, when there are too few features that can be detected in the scene, neither class of approach performs well, which prevents them from being used in practice. Nevertheless, a number of techniques have been published which applied feature-based 3D reconstruction in endoscopic sequences. \cite{Stoyanov2004} presented a method for dense depth recovery from stereo laparoscopic images of deformable soft-issue. \cite{Mourgues2001} proposed a correlation-based stereo method for surface reconstruction and organ modelling from stereo endoscopic images. 

\cite{Quartucci2000} applied a shape-from-shading technique to estimate the surface shape by recovering the depth information from the surface illumination. \cite{Mountney2007} proposed a probabilistic framework for selecting the most discriminative descriptors by Bayesian fusion method to compare twenty-one different descriptors. \cite{Wang2008} used scale-invariant feature transform (SIFT) features for endoscopy sequences and used the adaptive scale kernel consensus for robust motion estimation. \cite{Wu2008} also tracked SIFT features and utilised an iterative factorisation method for structure estimation. \cite{Mountney2006} presented a technique to construct a 3D map of the scene for MIS endoscopic surgery while recovering the camera motion based on \gls{SLAM} from a stereo endoscope, but their main focus was surface reconstruction.  

It is worth noting that state-of-the-art sparse feature-based visual SLAM systems such as \gls{PTAM} have achieved significant success in real-time camera tracking and mapping for real scenes with decent accuracy~\citep{Klein2007}. \cite{Newcombe2010} also utilised \gls{PTAM} to obtain camera poses for dense surface reconstruction. However, with the features available in endoscopic images, the performance of 3D reconstruction or registration is much worse due to deformation, specularity and textureless regions.

\section{Multi-view 3D to 2D image registration}
When registering a 3D model to multiple 2D views, many robust features are tracked in the scene and the camera tracking and scene reconstruction are calculated in separate parallel threads. The main role of \gls{PTAM} is to provide the camera poses for a number of video frames. The idea of incorporating colour-consistency with \gls{PTAM}'s camera tracking for 3D to 2D image registration is shown in Figure~\ref{fig:ptam_colour_consistency}. 

\begin{figure}[t]
    \centering
    \includegraphics[width=0.65\textwidth]{../materials/figs/ch3/photo_consistency.pdf}
    \caption[Multi-view 3D to 2D image registration.]{Colour-consistency with \gls{PTAM}'s camera tracking for 3D to 2D images registration.}
    \label{fig:ptam_colour_consistency}
    \hrulefill
\end{figure}

\subsection{Colour-consistency cost function}
The $i$-th 3D point $\mathbf{P}_{i}\in \mathbf{\Psi}$ in the visible point set $\mathbf{\Psi}$ of a 3D model and its corresponding 2D points $\mathbf{p}_{i,j}$ in the $j$-th camera projection image $\mathcal{I}_{j} \in \mathbf{\Omega}_{j}$ should have a similar colour. This property is called colour-consistency. Lighting and reflectance of the surface are significant factors in making the colour-consistency assumption. For preliminary studies, we take the Lambertian model that considers purely ambient lighting so we have an ideal environment to examine the proposed approach. 

The colour-consistency cost function is defined by projecting all of the visible points $\mathbf{P}_{i}$ into each camera image $\mathcal{I}_{j}$ using the camera pose $\mathbf{T}_{j} \in \mathbb{SE}(3)$ and the model pose $\mathbf{T}_{m} \in \mathbb{SE}(3)$:

\begin{align}
    E_{cc} = \sum_{j}\sum_{\mathbf{P}_{i}\in\mathbf{\Psi}}\left( \mathcal{I}_{j}(\pi(\mathbf{K}\mathbf{T}_{j}\mathbf{T}_{m}\mathbf{P}_{i})) - \bar{\mathcal{I}}(\mathbf{T}_{m}\mathbf{P}_{i}) \right)^{2}\label{eq:colour_consistency_cost_func}.
\end{align}

\noindent Taking all the camera images into account, the function $\bar{\mathcal{I}}$ averages the colour of the point $\mathbf{P}_{i}$ with a model pose $\mathbf{T}_{m}$. The projection function $\pi$ projects a 3-vector homogeneous point to 2D space. We assume that the camera is calibrated in advance so the intrinsic matrix $\mathbf{K}$ is known. Note that we stack the RGB intensities to form a long image vector $\mathcal{I}$.

The term inside the squared function in Equation~\ref{eq:colour_consistency_cost_func} is essentially the colour variance of each point $\mathbf{P}_{i}$. In other words, the colour-consistency is established by minimising the colour variance over all the visible points with respect to the model pose $\mathbf{T}_{m}$. The camera pose estimates $\mathbf{T}_j$ are provided by \gls{PTAM} so they are constant during the registration.

\subsubsection{Visibility detection}

In Equation~\ref{eq:colour_consistency_cost_func}, only 3D points that are visible in at least two camera images are taken into account. For each point on the model surface, we first need to calculate whether it is visible in each keyframe~\citep{Spoerk2012}. To achieve this we set a surface colour for each vertex, where the colour $[R,G,B]^{\top}$ has been set to the position $[x,y,z]^{\top}$ as shown in Figure~\ref{fig:phantom_rgb}. By rendering the object from each keyframe position we can limit ourselves to the visible front face simply by checking that the projected colour matches the colour of the vertex. As shown in Figure~\ref{fig:visibility_test}, the colour of the vertex at the front side is different from the back side. This turns out to be much more efficient than $z$-buffer methods which require us to calculate the distance to the vertex in each of the images. 

\subsubsection{Validation of colour-consistency assumption}
To investigate whether the designed colour-consistency is optimisable, we conducted a synthetic test using  textured bladder and pelvis phantom models shown in Figure~\ref{fig:phantom_raw}. The phantom models were obtained by \gls{CT} and we textured them with real \gls{RALP} endoscopic images. The synthetic textured phantom models are shown in Figure~\ref{fig:phantom_tex}. With the textured models, we make a simulation video where the true camera poses $\mathbf{T}_{j}$ are known. The synthetic video can be watched online\footnote{Synthetic video: \url{http://youtu.be/UyLnC6De1kw}}.

The goal of minimising the Equation~\ref{eq:colour_consistency_cost_func} with respect to the model pose $\mathbf{T}_{m}$ is to accurately align the model onto every 2D projection image. Using the simulation video and the camera poses, we devised a cost function test which intentionally moves the model pose $\mathbf{T}_m$ with respect to each motion parameter apart from the ground truth in $[-20, 20]$ mm and $[-30, 30]$ $^{\circ}$ with $0.1$ interval for translation and rotation respectively. Figure~\ref{fig:colour_consistency_gt_test} reveals that the cost function has a clear global minimum in the investigated ranges, and with more camera frames included the cost function tends to be smoother.

\begin{figure}[t!]
    \centering
    \hfill\null
    \subfloat[\label{fig:phantom_raw}]{\includegraphics[width=0.46\textwidth]{../materials/figs/ch3/phantom_raw.png}}
    \hfill
    \subfloat[\label{fig:phantom_tex}]{\includegraphics[width=0.46\textwidth]{../materials/figs/ch3/phantom_texture.png}}     
    \hfill\null \\
    \hfill\null
    \subfloat[\label{fig:phantom_rgb}]{\includegraphics[width=0.46\textwidth]{../materials/figs/ch3/phantom_rgb.png}}
    \hfill
    \subfloat[\label{fig:visibility_test}]{\includegraphics[width=0.46\textwidth]{../materials/figs/ch3/phantom_visible_point_detection.pdf}}
    \hfill\null
    \caption[Making a ground truth model texturing.]{(a) The ground truth phantom models of bladder and pelvis. (b) Their appearance after being textured. (c) The colourised model for detecting visible front face vertices. (d) The proposed fast visible point detection algorithm. Both cyan (front) and orange (back) points are projected onto the same position in the camera image, and can be distinguished by the vertex colour.}
    \hrulefill
\end{figure}

\begin{figure}[t!]
    \centering
    \hfill\null
    \subfloat[\label{fig:gt_test_2_trans}]{\includegraphics[width=0.46\textwidth, trim = 0.5cm 0cm 1.0cm 0.5cm, clip]{../materials/figs/ch3/2_frame_translation.eps}}
    \hfill
    \subfloat[\label{fig:gt_test_2_rot}]{\includegraphics[width=0.46\textwidth, trim = 0.5cm 0cm 1.0cm 0.5cm, clip]{../materials/figs/ch3/2_frame_rotation.eps}}     
    \hfill\null \\
    \hfill\null
    \subfloat[\label{fig:gt_test_10_trans}]{\includegraphics[width=0.46\textwidth, trim = 0.5cm 0cm 1.0cm 0.5cm, clip]{../materials/figs/ch3/10_frame_translation.eps}}
    \hfill
    \subfloat[\label{fig:gt_test_10_rot}]{\includegraphics[width=0.46\textwidth, trim = 0.5cm 0cm 1.0cm 0.5cm, clip]{../materials/figs/ch3/10_frame_rotation.eps}}
    \hfill\null
    \caption[Colour-consistency cost function validation.]{The validation results produced by rotating and shifting the 3D model with ground truth camera poses using 2 camera images in (a) and (b) and using 10 camera images in (c) and (d).}
    \label{fig:colour_consistency_gt_test}
    \hrulefill
\end{figure}

\subsubsection{Optimisation}
To optimise the pose $\mathbf{T}_{m}$ of the 3D model, a derivative-free algorithm is adopted. Having tried classical derivative-free approaches, we found Powell's bound optimisation by quadratic approximation (BOBYQA) has better performance than the principal axis (PRAXIS) and simplex methods~\citep{Powell2009}. In addition, \gls{BOBYQA} provides bound-constrained optimisation which can restrict the search within a reasonable capture range. The optimisation is conducted for the 6-vector $\mathbb{SE}(3)$ of the rigid body transformation $\mathbf{T}_{m}$.

\subsection{Empirical studies}
Experiments were run on an Intel(R) Core(TM) 2 Quad 2.5 GHz CPU with 4 GB physical memory and a Nvidia GeForce GT 330 graphic card with 1 GB global memory. The \gls{PTAM} system is provided by the original work of~\cite{Klein2007}\footnote{PTAM: \url{http://www.robots.ox.ac.uk/~gk/PTAM}}, which is used to evaluate camera poses. The \gls{BOBYQA} algorithm for registration is provided by an open source library\footnote{NLopt: \url{http://ab-initio.mit.edu/wiki/index.php/NLopt_Algorithms}}. The rest of programs are implemented in C++ and CUDA together with VTK for visualisation. 

\subsubsection{Evaluation of PTAM's camera tracking}
PTAM tracks camera positions on the fly by simultaneously tracking sparse features and mapping scenes. To validate the accuracy of using PTAM's camera pose, we ran PTAM on the simulation video to obtain the tracked camera poses as shown in Figure~\ref{fig:ptam_2d}~and~\ref{fig:ptam_3d}, and then, such estimated camera poses were compared with the simulation video's camera poses. Since PTAM's coordinate system is defined by using a manual stereo initialisation, to fairly compare them, we used Procrustes analysis algorithm~\citep{Schonemann1966, Horn87, Bookstein1997} to carry out coordinate system registration with respect to the rigid and scale transformation. Figure~\ref{fig:ptam_gt_2d}~and~~\ref{fig:ptam_gt_3d} show the result after PTAM's camera positions are transformed into the ground truth coordinate system. The overall \gls{RMSE} is $0.5$ mm. Note that although the pose errors are small, they will be propagated to the registration and break the colour-consistency assumption.

\begin{figure}[t!]
    \centering
    \hfill\null
    \subfloat[\label{fig:ptam_2d}]{\includegraphics[width=0.46\textwidth]{../materials/figs/ch3/ptam_2d.png}}
    \hfill
    \subfloat[\label{fig:ptam_3d}]{\includegraphics[width=0.46\textwidth]{../materials/figs/ch3/ptam_3d.png}}
    \hfill\null \\
    \hfill\null
    \subfloat[\label{fig:ptam_gt_2d}]{\includegraphics[width=0.46\textwidth, trim = 0.5cm 0cm 0.5cm 0.5cm, clip]{../materials/figs/ch3/ptam_gt_xy_view.eps}}
    \hfill
    \subfloat[\label{fig:ptam_gt_3d}]{\includegraphics[width=0.46\textwidth, trim = 0.5cm 0cm 0.5cm 0.5cm, clip]{../materials/figs/ch3/ptam_gt_3d_view.eps}}
    \hfill\null
    \caption[PTAM on simulation video.]{(a) Using PTAM for camera tracking on the simulation video. (b) PTAM's camera keyframes and the extracted features in a 3D view. (c) The camera poses of PTAM's keyframes compared with the ground truth. (d) The 3D view of (c). The overall pose \gls{RMSE} is $0.5$ mm.}
    \label{fig:ptam_gt_test}
    \hrulefill
\end{figure}

\subsubsection{Evaluation of the model registration}
We conducted an experiment which changes the ground truth pose by using additive white Gaussian noise (AWGN) with different standard deviations. Under each standard deviation, we ran the registration 500 times using a random set of 2, 5, 10 and 20 PTAM camera keyframes. A three-layer pyramid suggested by~\citep{Maes1999} was used for the derivative-free optimiser (see Section~\ref{eq:large_displacement_motion}). If the target registration error (TRE), which is defined as \gls{RMSE} of the entire vertices, is less than 2 mm, we regarded the registration process as having converged. 

Figure~\ref{fig:con_freq_no_inter}~and~\ref{fig:con_freq_inter} show the results of the frequency of convergence when the projection point colours are calculated by nearest neighbour and by bilinear interpolation respectively. The performances do not show much difference, and curves in each case share common trends. One can see that when only 2 keyframes are used, the frequency of convergence drastically decreases. When using 20 keyframes, the frequency of convergence starts to drop down after 3 mm standard deviation which is about the range in $\pm9$ mm/$^{\circ}$. Involving more keyframes may result in much better performance, but it also introduces more computation effort. Table~\ref{tab:statistics_regist_model} shows the statistics of the required optimising iteration and the running time for the converged cases. Note that the iteration time is accumulated by the number of iterations the optimiser takes in each layer. The average numbers of iteration are almost consistent no matter how many keyframes are used. This is because the required iteration of a derivative-free optimiser is largely affected by the number of parameters, and here we have 6 parameters in all cases.

\newcommand{\specialcell}[2][c]{%
  \begin{tabular}[#1]{@{}c@{}}#2\end{tabular}}
\begin{table}[t!]
    \centering
    \caption{Average running time and required iteration time for optimization with only the model pose}
    \label{tab:statistics_regist_model}
    \begin{tabular}{cccc}
    Keyframe \# & Avg. iteration & \specialcell{Avg. running time \\ per iter. (sec.)} & \specialcell{Avg. running time \\ (sec.)} \\
    \hline
    2  & 130 & 0.05 & 6.75 \\
    5 & 149 & 0.12 & 14.32 \\
    10 & 151 & 0.20 & 30.48 \\
    20 & 154 & 0.40 & 62.06 \\
    \hline
    \end{tabular}
\end{table}

\begin{figure}[t!]
    \centering
    \hfill\null
    \subfloat[\label{fig:con_freq_no_inter}]{\includegraphics[width=0.46\textwidth]{../materials/figs/ch3/conver_rate_no_interpolation.eps}}
    \hfill
    \subfloat[\label{fig:con_freq_inter}]{\includegraphics[width=0.46\textwidth]{../materials/figs/ch3/conver_rate_interpolation.eps}}
    \hfill\null
    \caption[Registration convergence frequency.]{Optimisation convergence frequency of the proposed approach using nearest neighbour interpolation (a) and using bilinear interpolation (b) for the projection point colour.}
    \label{fig:con_freq}
    \hrulefill
\end{figure}


\subsubsection{Registration in real endoscopic sequences}
We manually align the 3D model with the camera images to initialise the model pose. Although in the simulation scene PTAM has shown an accurate tracking result (Figure~\ref{fig:ptam_gt_test}), it turns out that PTAM cannot provide accurate camera poses when the scenes are more challenging, where there are many specular highlights or tissue deformation occurs. In addition, the invasion of surgical instruments will make the tracking fail. Therefore, we conclude that PTAM is not robust enough to provide multi-view camera poses in the endoscopic scenes. 

Given that the da Vinci robotic platform equipped with a stereo endoscopic camera, we can potentially use only two views to register the model. The stereo vision can provide an instant static snapshot for registration. As the convergence frequency may be poor when using only two images (Figure~\ref{fig:con_freq}), we will have to investigate the final registration result and re-run the registration with different initial model poses. This, however, is not practical for an augmented reality system for clinical use since the response time can be extremely critical during surgery.

The registration of a preoperative model with real clinical endoscopic scenes was eventually unsuccessful. To approach augmented reality, this methodology is implausible primarily due to the lack of geometry consistency and the difficulty of performing robust camera tracking to provide accurate camera poses in advance in endoscopic scenes. The computation time as shown in Table~\ref{tab:statistics_regist_model} is also far from real-time performance. Nevertheless, this does not prevent us from using the registered model to perform dense camera tracking.

\section{Dense monocular camera tracking}
We have shown that a non-textured 3D model can be registered with a number of 2D camera images. Once the model is properly registered, it can be textured by \textit{projective texturing} by using \gls{OpenGL}. Given a textured 3D model, a real-time dense 2.5D camera tracking can then be performed. Note that this also makes the assumption of a rigid-scene, which is a common limitation of general visual \gls{SLAM} systems such as \gls{PTAM}. But with a considerable number of measurements being present, such a dense tracking methodology turns out to be much more robust than a sparse feature based tracking system, even without compromising with real-time performance thanks to the availability of novel \gls{GPGPU} architectures~\citep{Newcombe2011a}.

\subsection{Photometric cost function}
The dense camera tracking can be achieved by projecting the textured model and registering the projection image with the current video image. It follows that the textured 3D model is projected into a virtual image $\mathcal{I}_{v}$ and the current image $\mathcal{I}_{c}$ is warped with respect to the camera pose $\mathbf{T}_{cv}$ to register with the virtual image. The photometric cost function is thus defined as

\begin{align}
    E_{dt} = \sum_{\mathbf{P}\in\mathbf{\Psi}_v}\left(\mathcal{I}_c\big(\pi(\mathbf{K}\mathbf{T}_{cv}\mathbf{P})\big) - \mathcal{I}_v \right)^{2}.
    \label{eq:dense_tracking_cost_func}
\end{align}

\noindent Once the photometric cost function is minimised, the optimal camera pose $\mathring{\mathbf{T}}_{cv}$ can be found that best registers the live image with the virtual image. We use the Lucas-Kanade style 2.5D forward compositional algorithm (FCA) to minimise Equation~\ref{eq:dense_tracking_cost_func} with respect to the $\mathbf{T}_{cv}$ using its minimal parameterisation $\mathfrak{se}(3)$ 6-vector (see Section~\ref{sec:parametric_optimisation} for the algorithm details). 

The visible point set $\mathbf{\Psi}_{v}$ will vary with the camera motion. Therefore we will have to update the virtual image at a certain stage to ensure there are enough visible points to form the virtual image. In practice the 3D points do not need to be from the model vertices. Instead, we exploit the \gls{OpenGL} rendering pipeline to get a dense 3D point set on the interpolated smooth surface.

\subsection{Empirical studies}
Experiments were run on an Intel(R) Core(TM) 2 Quad 2.5 GHz CPU with 4GB physical memory and a Nvidia GeForce GT 330 graphics card with 1 GB global memory. The dense tracking algorithm (\gls{FCA}) is implemented in CUDA, which is able to run at 100ms per $720\times576$ frame.

\subsubsection{Synthetic ground truth study}

In the \gls{RALP}, the arch of the pubic bone is the largest rigid structure that can be seen in the endoscopic images. The pubic bone model is manually segmented from the preoperative \gls{CT} scan and registered using the algorithm proposed in the previous section. To investigate the accuracy of dense tracking, we again conducted a synthetic tracking test where we simulate a video sequence using a textured pubic bone segmented from a textured phantom model shown in Figure~\ref{fig:real_tex_phantom}.

In order to obtain a realistic camera trajectory, we first tracked the real phantom video using \gls{PTAM}. This trajectory was then taken as the ground truth to generate the synthetic camera trajectory for the textured model as shown in Figure~\ref{fig:real_tex_segmented_phantom} to generate a simulation video. Dense tracking was then performed on the simulation video and the result was compared with the ground truth, as shown in Figure~\ref{fig:syn_dense_tracking}. The \gls{RMSE} is 0.33 mm overall. One can also qualitatively judge that the tracking path is very close to the ground truth. This shows that with ideal conditions, the dense tracking can be extremely accurate.

\begin{figure}[t!]
    \centering
    \hfill\null
    \subfloat[\label{fig:real_pubic_phantom}]{\includegraphics[width=0.3\textwidth]{../materials/figs/ch3/real_tex_phamton.eps}}
    \hfill
    \subfloat[\label{fig:real_nontex_segmented_phantom}]{\includegraphics[width=0.3\textwidth]{../materials/figs/ch3/real_nontex_chopped_phantom.eps}}
    \hfill
    \subfloat[\label{fig:real_tex_segmented_phantom}]{\includegraphics[width=0.3\textwidth]{../materials/figs/ch3/real_tex_chopped_phantom.eps}}
    \hfill\null
    \caption[Segmented pubic boan phantom.]{The phantom model used for the synthetic tracking test. The real left camera scene (a); its \gls{CT} scan model after manual segmentation (b); the model after being projectively textured by using the image (a).}
    \label{fig:real_tex_phantom}
    \hrulefill
\end{figure}

\begin{figure}[t!]
    \centering
    \hfill\null
    \includegraphics[width=0.8\textwidth]{../materials/figs/ch3/trajectory_result.eps}
    \hfill\null
    \caption[Synthetic dense tracking test.]{Synthetic dense tracking test showing a \gls{RMSE} $0.33$ mm.}
    \label{fig:syn_dense_tracking}
    \hrulefill
\end{figure}

\subsubsection{Real sequence study}
We manually align the pubic bone model with the camera images to initialise the model pose and perform registration using the method proposed in the previous section. Figure~\ref{fig:real_phantom_ar} shows a phantom alignment example, where the camera tracking is done by aligning the pubic bone frame-by-frame. The tracking result turns out to be quite robust with this rigid phantom. On the other hand, Figure~\ref{fig:real_endo_ar} shows the result in real endoscopic scene. The tracking can still work but the visibility of the pubic bone is limited. Sometimes it is even barely seen. In addition, the pubic bone model has an obviously different geometric shape compared to the real endoscopic appearance due to the presence of layers of soft tissue over the bone. These issues make the tracking very unstable and the camera will drift quickly. Figure~\ref{fig:real_failed_ar} also shows a failed tracking case where there are motions from surgical instruments. Since the model is textured by the camera images, if the scene is not static, the texture will not be consistent between the model and the scene in different frames and dense tracking consequently fails. For better understanding of the \gls{AR} demos, please watch the online video\footnote{\url{http://youtu.be/fPWp8XiurCI}}.

\begin{figure}[t!]
    \centering
    \hfill\null
    \subfloat[\label{fig:real_phantom_ar}]{\includegraphics[width=0.3\textwidth]{../materials/figs/ch3/real_phantom_ar.png}}
    \hfill
    \subfloat[\label{fig:real_endo_ar}]{\includegraphics[width=0.3\textwidth]{../materials/figs/ch3/real_endo_ar.png}}
    \hfill
    \subfloat[\label{fig:real_failed_ar}]{\includegraphics[width=0.3\textwidth]{../materials/figs/ch3/real_failed_ar.png}}
    \hfill\null
    \caption[Augmented reality in real video sequence.]{(a) The phantom video with the augmented bladder and pelvis models; (b) The real video of \gls{RALP} with the augmented models of pelvic rim, prostate, urethra and rectum. (c) A failed tracking example where the tracked pubic bone model moves with the instrument motion.}
    \label{fig:real_Ar}
    \hrulefill
\end{figure}

\section{Discussions and conclusions}
In this chapter, we have presented an approach to registration of a preoperative 3D model to intraoperative endoscopic video which combines PTAM tracking with colour-consistency registration incorporating a fast calculation of the visible 3D surface, followed by a preliminary attempt for dense monocular camera tracking. To validate the method we developed simulation test datasets with ground truth camera trajectories and textured models. Although the monocular dense tracking showed promisingly robust and real-time performance, the proposed registration scheme is however problematic in real clinical cases in terms of both practice and theory.

\subsubsection{Registration between two different geometries}
The 3D to 2D registration using colour-consistency is an over-optimistic idea. In practice, a segmented preoperative model does not necessarily have a geometric shape exactly the same as what can be seen in the endoscopy. This can be due to errors sourced from the \gls{CT}/\gls{MRI} scan and the segmentation algorithm. The errors will be accumulated and propagated to the final 3D model. If the surface geometries are not the same, the multi-view colour-consistency assumption between the 3D model and the 2D images will not be hold.

A promising algorithm for relaxing the geometry constraint for a 3D to 2D non-textured model registration is to use a soft probabilistic method in which the 3D model plays a role to provide foreground/background masks to define the statistical appearance models, and the registration is performed by pixel-wise \gls{MAP} estimation~\citep{Prisacariu2012}. But this scheme requires the foreground and background statistical models to have distinct probability distributions, which may not be the case for our surgical images. This is something we will investigate in the near future. 

\subsubsection{The impracticality of registering rigid models using PTAM for real-time augmented reality}
We have also shown that PTAM can provide accurate camera tracking only within ideal scenes where there is no deformation and there are enough good features to track. Such ideal scenes are practically not available for real endoscopic scenes. Furthermore, the instrument motions make \gls{PTAM} easily fail. As a result, the sparse feature version of visual \gls{SLAM} systems are not suitable for use in \gls{RALP} endoscopic scenes.

Even if we have a number of decently tracked camera poses, the registration process is still far from real-time performance as shown in Table~\ref{tab:statistics_regist_model}. Although the cost function in Equation~\ref{eq:colour_consistency_cost_func} can instead be locally linearised and be optimised by efficient Lucas-Kanade-style algorithms which will be introduced in Chapter~\ref{ch:real_time_sense_stereo_camera_tracking}, the unestablished colour-consistency assumption will anyway make the registration failed in the end. In short, we had just ill-conditioned the problem.

\subsubsection{Redirection}
Instead of registering the preoperative models with the camera images, we can directly reconstruct the 3D structure of the scene using a stereo endoscopic camera. In this way, the stereo reconstruction provides an instant 3D textured model that is useful for further dense tracking. Given that the-state-of-the-art stereo reconstruction algorithms have demonstrated high quality and real-time performance, this is a promising approach to achieve an efficient and effective tracking system for image-guided endoscopic scenes. We therefore redirected our approach to the problem. Chapter~\ref{ch:real_time_dense_stereo_reconstruction} will introduce a real-time dense stereo reconstruction algorithm. The reconstructed model is then used for stereo camera tracking as described in Chapter~\ref{ch:real_time_sense_stereo_camera_tracking}.


