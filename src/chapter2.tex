%\begingroup
\chapter{Preliminaries}
\label{ch:preliminaries}
%\renewcommand{\baselinestretch}{1}
\minitoc
%\endgroup

\newpage

\section{Introduction}
This chapter describes materials and mathematical concepts that will be used throughout this thesis. Notation used here will be complied with as much as possible in the subsequent chapters. One key factor to achieve an efficient image guidance system is the use of 3D graphics programming which has been significantly improved in terms of both algorithms and hardware architectures in the last few decades by researchers from the computer graphics community. Implementation details for a real-time image guidance system are also described here. I recommend that readers study this chapter before proceeding the remainder of the thesis.

\section{Coordinate system}

Coordinate systems or coordinate frames are the foundation of analytic geometry. Works in this thesis use a Cartesian coordinate system to span $\mathbb{R}^{n}$ to represent $n$-dimensional data. A Cartesian coordinate system specifies each point uniquely in $\mathbb{R}^{n}$ space by a $n$-vector in which each entry in the vector is the signed distance from the point to $n$ mutual perpendicular axes (basis vectors), measured in the same unit of length. The point where all axes intersect is the world origin which is denoted by a $n$-dimensional zero vector $\mathbf{0}$.

In a $\mathbb{R}^{n}$ space, points are defined with respect to the \textit{reference frame} which consists of the basis vectors. For example, points $a=(1,1)$, $b=(3,2)$ and $c=(-1,-4)$ in $\mathbb{R}^{2}$ can be shifted to $a=(0,0)$, $b=(2,1)$ and $c=(-2,-5)$ if the point $a$ is regarded as the world origin of the reference frame. Rigid transformation between each local coordinate system is possible. When interacting with different local frames, careful attention to coordinate definition is required, particularly when we are dealing with 3D graphical data which may be defined in \textit{world coordinates}, \textit{camera coordinates} and \textit{model coordinates}. It becomes surprisingly difficult to figure out what is wrong once the coordinate systems are messed up.

3D coordinate systems can be expressed by the special Euclidean $\mathbb{SE}(3)$ Lie group. Its minimum parameterisation is $\mathbf{x}\in\mathfrak{se}(3)$ Lie algebra in which it is a 6-vector $\mathbf{x} = (\nu, \omega) \in \mathbb{R}^{6}$ consisting of $\nu \in \mathbb{R}^{3}$ for the linear velocity and $\omega \in \mathfrak{so}(3)$ for the angular velocity of the motion. The smooth and invertible rigid-body $4 \times 4$ transformation matrix $\mathbf{T} \in \mathbb{SE}(3)$ can be obtained by the exponential map of $g(\mathbf{x})$:


\begin{align}
   \mathbf{T}(\mathbf{x}) = \mathrm{exp}\big(g(\mathbf{x})\big) = 
        \left(\begin{array}{ll}
            \mathbf{R} & \mathbf{t} \\ 
            \mathbf{0}^{\top} & 1
        \end{array}\right) \in \mathbb{R}^{4\times4}, \nonumber
\end{align}

\noindent where $\mathbf{R} \in \mathbb{SO}(3)$ and $\mathbf{t} \in \mathbb{R}^{3}$. See Appendix~\ref{app:lie_group} for details of Lie groups. 

\subsection{3D coordinate convention}
Without loss of generality, we define the camera coordinate system with OpenGL's convention, which is a right-handed coordinate system with its origin located at the optical center as shown in Figure~\ref{fig:coordinate_convention}. The positive $x$-axis points right. The positive $y$-axis is defined as the up vector of the camera. The negative $z$-axis has the same direction as the camera faces and coincides with the optical axis. The 2D image coordinate has an origin at top-left corner where the $u$-axis points right and $v$-axis points down.

\subsection{Transformation between coordinates}
We use a notation $\mathbf{T}_{\{\cdot\}}$ to denote an individual frame and $\mathbf{T}_{\{a\}\{b\}}$ to indicate transformation from $b$ frame to $a$ frame. With this notation, it is crystal clear that a world frame $\mathbf{T}_{w}$ transformed to a model frame $\mathbf{T}_{m}$ by $\mathbf{T}_{mw}$, followed a transformation $\mathbf{T}_{cm}$ from the model frame to a camera frame, is equivalent to the transformation $\mathbf{T}_{cw}$ from the world frame to the camera frame. Figure~\ref{fig:coordinate_transform} shows the relationship. Sometimes we need to transform coordinates in the forward or backward direction. The inverse of $\mathbf{T}$ has a closed form 

\begin{align*}
   \mathbf{T}^{-1} = \left(\begin{array}{cc}
                \mathbf{R}^{\top} & -\mathbf{R}^{\top}\mathbf{t} \\ 
                \mathbf{0}^{\top} & 1
                \end{array}\right).
\end{align*}

\noindent Given a transformation $\mathbf{T}_{cw}$, the optical center position of a camera frame with respect to the world frame can be inferred as $-\mathbf{R}_{cw}^{\top}\mathbf{t}_{cw}$.

\begin{figure}[t]
    \centering
    \hfill\null
    \subfloat[\label{fig:coordinate_convention}]{\includegraphics[width=0.5\textwidth]{../materials/figs/ch2/coordinate_conv.pdf}}
    \hfill
    \subfloat[\label{fig:coordinate_transform}]{\includegraphics[width=0.5\textwidth]{../materials/figs/ch2/coordinates.pdf}}
    \hfill\null
    \caption[Coordinates system.]{(a) Coordinate convention used in this thesis. (b) Coordinates transformation notation. }
    \label{fig:coordinates_transform}
    \hrulefill
\end{figure}

A 3D point $\mathbf{P}_{w}=(x,y,z)^{\top}$ in the world frame can be transformed to the camera frame using the homogeneous representation

\begin{align*}
    \mathbf{P}_{c} = \pi(\mathbf{T}_{cw}\dot{\mathbf{P}}_{w}),
\end{align*}

\noindent where the $\dot{\{\,\}}$ operator simply appends $1$ in the augmented vector, so $\dot{\mathbf{P}}_{w}=(x,y,z,1)^{\top}$ and the projection function $\pi:\mathbb{R}^{n} \rightarrow \mathbb{R}^{n-1}$ is defined as

\begin{align*}
    \pi(a) = \frac{1}{a_{n}}\left(\begin{array}{c}
                                            a_{1} \\
                                            \vdots \\
                                            a_{n-1}\end{array}\right).
\end{align*}



\section{Pinhole camera model}
The most common camera projection model is the \textit{pinhole camera model}, which projects a 3D point $\mathbf{P}$ in the camera frame to a 2D point $\mathbf{p}$ using a camera intrinsic matrix $\mathbf{K}$ such as

\begin{align*}
    \mathbf{p} = \pi(\mathbf{K}\mathbf{P}),
\end{align*}

\noindent where the $\mathbf{K}$ is

\begin{align*}
    \mathbf{K} = \left(\begin{array}{ccc}
                    f_{u} & \alpha & u_{0} \\
                    0 & f_{v} & v_{0} \\
                    0 & 0 & 1 \\
                    \end{array}\right).
\end{align*}

\noindent The 2D coordinates $u_{0}$ and $v_{0}$ is the \textit{principal point} position of the camera in 2D image space which is the center of projection or the nearest point on the image plane to the pinhole. In general this position does not need to be at exact and is often set to the image center. The $f_{u}$ and $f_{v}$ are the horizontal and vertical focal length scaling the projection pixel size $p_{w} \times p_{h}$. They assume the original constant bidirectional focal length $f$ that is related to  

\begin{align*}
    f_{u} = \frac{f}{p_{w}} \quad \mathrm{and} \quad f_{v} = \frac{f}{p_{h}},
\end{align*}

\noindent and in most camera sensors it gives $p_{w} \approx p_{h}$ (square pixel). The $\alpha$ is a skew parameter for shearing the image plane, which is usually ignored and set to zero. As will be used in Chapter~\ref{ch:real_time_sense_stereo_camera_tracking}, the inverse of $\mathbf{K}$ ($\alpha = 0$) is

\begin{align*}
    \mathbf{K}^{-1} = \left(\begin{array}{ccc}
                            \frac{1}{f_{u}} & 0 & -\frac{u_{0}}{f_{u}} \\
                            0 & \frac{1}{f_{u}} & -\frac{v_{0}}{f_{v}} \\
                            0 & 0 & 1 \\
                            \end{array}\right).
\end{align*}


\noindent We calibrate the camera intrinsic matrix $\mathbf{K}$ with a publicly available Matlab camera calibration toolbox\footnote{Camera Calibration Toolbox for Matlab: \url{http://www.vision.caltech.edu/bouguetj/calib_doc}} which also provides a function to do stereo camera rectification.

\section{Mathematical models for dense vision}
\label{sec:bayesian_inference_dense_vision}

In the literature dense methods are also referred as \textit{direct} methods since a vision problem is solved by analysing the intensity of each pixel directly. The most early examples of dense methods can be sourced back to \cite{Horn1981} and \cite{Lucas1981} in which they introduced dense algorithms to tackle different vision problems in concurrent streams, but they surprisingly ended up with similar principles and theoretical bases.

\subsection{Taxonomy of models}

In the early eighties, \cite{Horn1981} proposed to estimate general motions in 2D images at the pixel level, which is known as \textit{optical flow} in the literature. As such an inverse problem is ill-posed due to homogeneous areas and the \textit{aperture problem}\footnote{Aperture problem is a phenomenon that when we observe a moving slant line through an aperture, its local motion has ambiguity up to the two perpendicular directions of the perceived motion.}, as well as a sum of squared data term, they introduced an explicit regulariser term to smooth pixel flow vectors within a local neighbourhood and better constrain the problem. The regularised cost function was minimised by solving the Euler-Lagarnge equations. Their original work has had a significant impact on the development of variational approaches in dense vision. Subsequent work from others has mainly been dedicated to looking for a better regularisation scheme.

In contrast, \cite{Lucas1981} were trying to solve a stereo matching problem without an explicit regulariser. In their original paper, the photometric cost function was formulated pixel-wise as the sum of squared difference between a parametrically warped patch in one image and the target template patch in the other image. The optimisation was performed by locally linearising the cost function with respect to the parametric patch motion model and moving the parameters in the gradient descent direction using the Newton-Raphson method. With a rectified stereo rig, the correspondence search can be found along a 1D search line, whereas when the matching problem goes beyond 1D such as in a non-rectified case, the warping-patch scheme can be much more efficient than the naive exhaustive search. They also showed that for 2D optical flow estimation, the correspondences can be found by warping the patch to find the best displacement of $(u,v)$ in sub-pixel accuracy.

At a glance the two research streams look very different but in fact they share common assumptions and theoretical principles. They are both algorithms for image registration. They both used the sum of squared differences cost function to model the problems together with a Lambertian reflectance model to ensure lighting consistency between two images. In addition, although the original work of \cite{Lucas1981} did not explicitly model the local smoothness assumption, \cite{Lucas1984} later found that the local patch indeed implicitly behaves as a smooth regulariser in response to the window size. More interestingly, when we look them from Bayesian inference point of view, they are both a \textit{maximum a posteriori} (MAP) estimator.


Furthermore, \cite{Bergen1992} provided a clear categorisation for different models used in direct methods:

\subsubsection{Fully parametric models}
Lucas-Kanade-like methods solve a model fitting problem assuming all pixels undergoing a global parametric motion model such as translation, rotation, affine, homography, etc.

\subsubsection{Quasi-parametric models}
Between fully parametric and non-parametric, there are methods lying in quasi-parametric models where there is a prior 3D rigid object to be parameterised in order to constrain the pixel motion in the 2D image. These models can be applied to a 3D to 2D rigid image registration problem~\citep{Chang2012, Prisacariu2012}.

\subsubsection{Non-parametric models}
Horn-Schunck-like methods do not assume any specific global motion model but let each pixel have its own motion. Since the problem is highly under-constrained, in general the models require an explicit smooth regulariser term to be estimated together with a global variational function.

\subsection{Bayesian inference interpretation}
Since we are working with sensors to imagine scenes, the measurements always come along with some kind of uncertainties such as noise. Interpreting the problem in a statistical way is therefore beneficial. Assume that we would like to estimate a state vector $\boldsymbol{\xi}$ given a measurement vector $\boldsymbol{\mu}$. Treating the $\boldsymbol{\xi}$ and $\boldsymbol{\mu}$ as random variables enables us to analyse different hypotheses $\boldsymbol{\xi}$ so that the hypothesis with highest probability given the observation $\boldsymbol{\mu}$ will be the most likely solution we are looking for. 

The hypothesis can be analysed by \textit{maximum a posteriori} (MAP) estimation. Mathematically speaking this is equivalent to maximising the \textit{posterior}:

\begin{align}
    \mathring{\boldsymbol{\xi}} = \mathrm{arg}\,\underset{\boldsymbol{\xi}}{\mathrm{min}}\,\, p(\boldsymbol{\xi}|\boldsymbol{\mu}).
\end{align}

\noindent According to Bayesian inference theory, the conditional probability $p(\boldsymbol{\xi}|\boldsymbol{\mu})$ can be written as

\begin{align}
    p(\boldsymbol{\xi}|\boldsymbol{\mu}) = \frac{p(\boldsymbol{\mu}|\boldsymbol{\xi})p(\boldsymbol{\xi})}{p(\boldsymbol{\mu})},
    \label{eq:bayesian_conditional}
\end{align}

\noindent where $p(\boldsymbol{\xi})$ is the \textit{prior} and $p(\boldsymbol{\mu}|\boldsymbol{\xi})$ the \textit{likelihood} (also called \textit{data model}) expressing how well the measurements $\boldsymbol{\mu}$ can be explained by the state $\boldsymbol{\xi}$. The denominator $p(\boldsymbol{\mu})$ is known as the \textit{evidence} which normalised the conditional probability. Since it does not depend on $\boldsymbol{\xi}$, it can be ignored during the optimisation.

\subsubsection{MAP estimation for non-parametric models}
\label{sec:map_estimation_for_non_parametric_models}
From Equation~\ref{eq:bayesian_conditional} we can see that the performance of the MAP estimator is determined by the generative data model $p(\boldsymbol{\mu}|\boldsymbol{\xi})$ and the prior $\boldsymbol{\xi}$. In principle, if we had perfect data model and prior model, the \gls{MAP} estimation would find us a global optimum. However, the perfect models are unlikely to be achieved in practice since an ill-posed non-parametric model problem is in general very complicated. 

As we will be dealing with a reconstruction problem in Chapter~\ref{ch:real_time_dense_stereo_reconstruction}, let us assume $\boldsymbol{\xi} = \mathcal{D}$ where the state vector $\boldsymbol{\xi}$ is the disparity map we would like to estimate, and the pixel-wise measurement $\boldsymbol{\mu}$ as the initial disparity estimation from a simple stereo matching algorithm such as plane sweep, followed by a \gls{WTA} scheme. Assuming the initial disparity $\boldsymbol{\mu}$ has pixel-wise additive Gaussian noise and each measurement is \textit{independent and identically distributed} (i.i.d), the likelihood is

\begin{align}
    p(\boldsymbol{\mu}|\boldsymbol{\xi}) = \prod_{\boldsymbol{\xi}}\frac{1}{\sqrt{2\pi}\alpha}\mathrm{exp}(-\frac{(\boldsymbol{\mu} - \boldsymbol{\xi})^2}{2\alpha^{2}}).
    \label{eq:bayesian_likelihood}
\end{align}

\noindent The prior knowledge of a disparity map could be that the disparities should be smooth in a local region in terms of the first order quadratic smoothness assumption:

\begin{align}
    |\nabla \boldsymbol{\xi}| = \sqrt{\left(\frac{\partial \boldsymbol{\xi}}{\partial u}\right)^{2} + \left(\frac{\partial \boldsymbol{\xi}}{\partial v}\right)^{2}}.
\end{align}

\noindent Again we assume the prior is also a Gaussian distribution:

\begin{align}
    p(\boldsymbol{\xi}) = \prod_{\boldsymbol{\xi}}\frac{1}{\sqrt{2\pi}\beta}\mathrm{exp}(-\frac{ |\nabla \boldsymbol{\xi}|^2}{2\beta^{2}}).
    \label{eq:bayesian_prior}
\end{align}

\noindent Incorporating Equation~\ref{eq:bayesian_likelihood} and \ref{eq:bayesian_prior} into \ref{eq:bayesian_conditional}, we arrive at

\begin{align}
    p(\boldsymbol{\xi}|\boldsymbol{\mu})  = \prod_{\boldsymbol{\xi}}\frac{1}{2\pi\alpha\beta}\mathrm{exp}(-\frac{ |\nabla \boldsymbol{\xi}|^2}{2\beta^{2}} - \frac{(\boldsymbol{\mu} - \boldsymbol{\xi})^2}{2\alpha^{2}}).
    \label{eq:bayesian_posterior}
\end{align}

Performing \gls{MAP} estimation on Equation~\ref{eq:bayesian_posterior} is equivalent to minimising its negative-log form as a cost function:

\begin{align}
    E(\boldsymbol{\xi})  = \sum_{\boldsymbol{\xi}}\left\{\frac{1}{2}|\nabla \boldsymbol{\xi}|^2 + \frac{1}{2\lambda}(\boldsymbol{\mu} - \boldsymbol{\xi})^{2}\right\},
    \label{eq:tikhonov_model}
\end{align}

\noindent where the $\lambda$ is $\frac{\alpha^{2}}{\beta^{2}}$. Equation~\ref{eq:tikhonov_model} turns out to be a discrete configuration of Tikhonov regularisation~\citep{Tikhonov1943} which is a special form of Equation~\ref{eq:disp_global_func_continuous}. The prior acts as a regulariser and the likelihood is nothing but the data term. The $\lambda$ originally consists of the variances now acts like a parameter controlling the strength of the regulariser and data term. 

The derivation shows that the original Tikhonov variational model used in Horn-Schunck's work makes the assumption that the underlying data model and smoothness prior has a Gaussian distribution. However, this is generally not true. The probability distributions do not necessarily need to be Gaussian and we will discuss a few other better choices to model the inverse problem in Section~\ref{sec:disparity_computation_optimisation}.

\subsubsection{MAP estimation for parametric models}
Parametric models can be solved by the \gls{MAP} estimation in the similar way. As we will be dealing with the camera tracking problem in Chapter~\ref{ch:real_time_sense_stereo_camera_tracking}, let us assume $\boldsymbol{\xi}=\mathbf{x}$ where the state vector $\boldsymbol{\xi}$ is a 6-vector in $\mathfrak{se}(3)$. The $n$-pixel measurement is defined by the photometric residual between the warped current image $\mathcal{I}$ and the reference image $\mathcal{I}^{*}$:

\begin{align}
    \mu_{i}(\boldsymbol{\xi}) = \mathcal{I}(w(\boldsymbol{\xi}, \mathbf{p}_{i})) - \mathcal{I}^{*}(\mathbf{p}_{i}), \quad \mathrm{where} \quad i=1,...,n.
    \label{eq:bayesian_likelihood_per_param}
\end{align}

\noindent The warping function $w$ takes the global motion model $\boldsymbol{\xi}$ to transform all pixel $\mathbf{p}_{i}$. With the i.i.d property and assume the pixel-wise residual is a Gaussian distribution, the likelihood is

\begin{align}
    p(\boldsymbol{\mu}|\boldsymbol{\xi}) = \prod_{i}\frac{1}{\sqrt{2\pi}\gamma}\mathrm{exp}(-\frac{\mu_{i}^{2}}{2\gamma^{2}}), \quad \mathrm{where} \quad i=1,...,n.
    \label{eq:bayesian_likelihood_param}
\end{align}

The parametric model \gls{MAP} estimation can be conducted without considering prior knowledge, in which it is equivalent to only maximising the likelihood. The cost function form of Equation~\ref{eq:bayesian_likelihood_param} is therefore

\begin{align}
    E(\boldsymbol{\xi}) = \frac{1}{\sqrt{2\pi}\gamma}\sum_{i}\Big(\mathcal{I}(w(\boldsymbol{\xi}, \mathbf{p}_{i})) - \mathcal{I}^{*}(\mathbf{p}_{i})\Big)^{2}, \quad \mathrm{where} \quad i=1,...,n.
    \label{eq:bayesian_likelihood_neg_log_param}
\end{align}

\noindent Equation~\ref{eq:bayesian_likelihood_neg_log_param} is exactly the same cost function used in Equation~\ref{eq:quadri_tracking_energy}. Note that the coefficient term $\frac{1}{2}$ or $\frac{1}{\sqrt{2\pi}\gamma}$ does not affect the position of global minimum. The prior $p(\boldsymbol{\xi})$ can be introduced over the estimated camera motion of course. Possible choices include an additional sensor such as an \gls{IMU} (uniform prior), or prediction from a Kalman filter (Gaussian prior).

\section{Graphics and parallel computing}

When implementing a real-time \gls{IGS} system, it is crucial to leverage the computation resources for computer vision algorithms and \gls{AR} graphics. The work in this thesis relies heavily on novel graphics technologies in both software and hardware. 

Dense methods can significantly benefit from the increasing sophistication of computer graphics algorithms and graphics hardware. For example, when one wants to visualise the results with complex 3D objects, modern rendering algorithms provide optimised drawing-on-screen pipeline with hardware acceleration. Hence with proper graphics programming the \gls{IGS} system will not stall because of the complexity of visualisation tasks. In addition, when solving dense methods for computer vision tasks, parallel computation is the most intuitive and efficient method since most of operations are pixel-wise and given that the computational power of \gls{GPU} outperforms \gls{CPU} when the serial operations can actually be parallelised and all the element-wise operations proceed at once. The design principle is that the \gls{GPU}s have a parallel throughput architecture that in general emphasises executing many concurrent threads slowly, rather than executing a single thread very quickly. 

Furthermore, the gap between computer vision and computer graphics is tending to reduce. Sophisticated dense visual SLAM systems such as \gls{DTAM} have shown that by incorporating graphics algorithms into the computer vision tasks, the built dense map and tracking can seamlessly improve one another. Specifically, on one hand, the dense tracking needs the textured surface to be efficiently rendered in hyper real time, and on the other hand, the multi-view stereo reconstruction also requires the camera to be well tracked with the rendered keyframe textures.

\subsection{Open graphics library (OpenGL)}
\gls{OpenGL} is a cross-platform application programming interface (API) for rendering 2D and 3D vector graphics. The \gls{API} is designed to access the \gls{GPU}, to achieve hardware-accelerated rendering. The generic geometry primitives that \gls{OpenGL} provides include points, lines, triangle meshes, etc.

\gls{OpenGL} defines and implements a highly optimised rendering pipeline to efficiently render 3D geometries onto 2D windows on the screen. The pipeline is shown in Figure~\ref{fig:opengl_rendering_pipeline}. 3D geometry data are input to \textit{vertex shader} and transformed, projected and rasterised before arriving at the \textit{fragment shader} and in the end the pixel colours are assigned in the fragment shader and will be filled into the \textit{framebuffer}. 

Note that OpenGL combines the transformation of model and camera coordinates together in \gls{OpenGL}'s \texttt{ModelView} matrix. In practice we will never need to literally transform object vertices into world coordinates using $\mathbf{T}_{wm}$ since this can be computationally expensive if the number of vertices is large. Instead, by incorporating the model-to-world transformation matrix $\mathbf{T}_{wm}$ into the world-to-camera transformation $\mathbf{T}_{cw}$, we can obtain a \texttt{ModelView} matrix $\mathbf{T}_{cm}$ which simply transforms the 3D points from the model coordinates to camera coordinates, bypassing the world coordinates.


Beside the default on-screen framebuffer, \gls{OpenGL} also provides off-screen framebuffer rendering. The frame buffer object (FBO), vertex buffer object (VBO) and pixel buffer object (PBO) are particularly useful functions when we want to perform a render-to-texture operation and use the rendered textures to interact with computer vision tasks.  

\begin{figure}[t]
    \centering
    \includegraphics[width=1.0\textwidth]{../materials/figs/ch2/opengl_pipeline.pdf}
    \caption[OpenGL rendering pipeline.]{\gls{OpenGL} rendering pipeline.}
    \label{fig:opengl_rendering_pipeline}
    \hrulefill
\end{figure}

\subsection{OpenGL shading language (GLSL)}
\gls{OpenGL} is a kind of state machine which provides high performance rendering functions for 3D graphics, in which users have to set flags for a given rendering task. The \gls{OpenGL} \gls{API}s such as \texttt{glEnable()}, \texttt{glViewport()}, \texttt{glClear()}, etc, can set the rendering states but they do not allow users to control rendering approaches. This is where \gls{GLSL} plays a role. \gls{GLSL} is a high-level shading language based on the syntax of the C programming language. With \gls{GLSL} the original functions of the vertex shader and fragment shader become programmable. This enables users to implement task-driven rendering approaches without scarifying any performance. 

Figure~\ref{fig:glsl_rendering} shows rendering examples which are not possible to achieve by using the generic \gls{OpenGL} \gls{API}. Although in general \gls{GLSL} is used for better graphics rendering such as to realise Phong shading, programming shaders can also be very useful for computer vision tasks. For example, depth map can be used as assigning depth value to each pixel and a normal map provides a dense interpolated normal for each pixel.

\begin{figure}[t]
    \centering
    \subfloat[]{\includegraphics[width=0.5\textwidth]{../materials/figs/ch2/depth_img.png}}
    \subfloat[]{\includegraphics[width=0.5\textwidth]{../materials/figs/ch2/contour_img.png}}
    \caption[GLSL rendering.]{\gls{GLSL} rendering examples. The Stanford bunny in depth (a) and contour (b).}
    \label{fig:glsl_rendering}
    \hrulefill
\end{figure}

\subsection{Compute unified device architecture (CUDA)}
Compared with \gls{GLSL}, \gls{CUDA} is a more general programming language which allows users to program \gls{GPGPU} to exploit the parallel computation power of the \gls{GPU}. In a modern CUDA architecture, a CUDA core consists of several functional components as shown in Figure~\ref{fig:cuda_acrchi}. 

The most basic computation unit is a \textit{thread} which lies inside a \textit{block}. A number of blocks form a \textit{grid}. In a grid there are \textit{global memory}, \textit{texture memory} and \textit{constant memory} which are globally accessible (for all threads). In each block, there is a local memory called, \textit{shared memory}, which allows access for threads in the same block. 

When programming a \gls{CUDA} application, the processing data has to be uploaded from \gls{CPU} main memory to \gls{GPU} global memory so each thread in each block has access to it. One can see that it is not possible for threads across different blocks to share processing data. When implementing algorithms where a summation operation is involved, such as the vector inner product or the solver of the normal equations, a \textit{reduction} algorithm has to be considered~\citep{Sanders2010}.

\begin{figure}[t!]
    \centering
    \includegraphics[width=0.8\textwidth]{../materials/figs/ch2/cuda_archi.pdf}
    \caption[CUDA architecture.]{Modern \gls{CUDA} architecture. The texture and constant memory on each grid is read-only. After \gls{CUDA} compute capability $2.0$, it is possible to write the texture by using \textit{surface object}.}
    \label{fig:cuda_acrchi}
    \hrulefill
\end{figure}

\section{Summary}
This chapter has describes basic geometric and mathematical concepts about dense methods. The practical programming tools used in this thesis have also been introduced.
