\chapter{Lie groups and their algebra}
\label{app:lie_group}

In linear algebra, a group consists of any set $G$ and operation $\otimes$ that have the properties:

\begin{align*}
\raggedright
\mathbf{Closure} \quad         &  \forall a, b \in G,\, a \otimes b \in G. \\
\mathbf{Associativity} \quad   &  \forall a, b, c \in G,\, (a \otimes b) \otimes c = a \otimes (b \otimes c) \in G. \\
\mathbf{Identity} \quad        &  \exists a \in G, \forall b \in G: a \otimes b = b \otimes a = b, \,\, 
                                    \mathrm{and}\,\, a = 1_{G}. \\
\mathbf{Inverse} \quad         &  \forall a \in G, \exists b \in G: a \otimes b = b \otimes a = 1_{G}.
\end{align*}

A Lie group is any group which is also a finite dimensional smooth manifold, where the group operations of multiplication and inversion are smooth maps. Of particular interesting to us is that every Lie group has a correspondent Lie algebra which represents the tangent space in which the algebra expresses the derivative of the group's manifold~\citep{Rossman2002}. 

Parameterising a Lie group matrix with its algebra turns out to be a minimum parameterisation. This can significantly help in optimisation especially when we are consider incremental update scheme. Taking the \textit{special orthogonal} Lie group $\mathbb{SO}(3)$ which is a $3\times3$ rotation matrix as an example, instead of optimising all 9-element in the matrix in Lie group space, its Lie algebra $\mathfrak{so}(3)$ reveals that the optimisation only need to apply to the minimal 3-vector parameterisation.

The mapping between a Lie group and its algebra is performed by the \textit{exponential map} and \textit{matrix logarithm}. For example, a \textit{special Euclidean} Lie group $\mathbb{SE}(3) \in \mathbb{R}^{4\times4}$ matrix $\mathbf{T}$ can be mapped from its 6-vector Lie algebra $\mathbf{x} \in \mathfrak{se}(3)$ as

\begin{align*}
    \mathbf{T} = \mathrm{exp}(g(\mathbf{x})), \quad \mathrm{where} \quad g(\mathbf{x}) = \sum^{6}_{i=1} \mathbf{x}_{i}\mathbf{G}_{i}.
\end{align*}

\noindent The $\mathbb{SE}(3)$ generator $\mathbf{G}_{i}$ is

\begin{align*}
\mathbf{G}_{1} = \left(\begin{array}{cccc}
                            0 & 0 & 0 & 1 \\
                            0 & 0 & 0 & 0 \\
                            0 & 0 & 0 & 0 \\
                            0 & 0 & 0 & 0
                        \end{array}
                 \right)
\mathbf{G}_{2} &= \left(\begin{array}{cccc}
                            0 & 0 & 0 & 0 \\
                            0 & 0 & 0 & 1 \\
                            0 & 0 & 0 & 0 \\
                            0 & 0 & 0 & 0
                        \end{array}
                 \right)                 
\mathbf{G}_{3} = \left(\begin{array}{cccc}
                            0 & 0 & 0 & 0 \\
                            0 & 0 & 0 & 0 \\
                            0 & 0 & 0 & 1 \\
                            0 & 0 & 0 & 0
                        \end{array}
                 \right)                 
\\\\
\mathbf{G}_{4} = \left(\begin{array}{cccc}
                            0 & 0 & 0 & 0 \\
                            0 & 0 & 1 & 0 \\
                            0 &-1 & 0 & 0 \\
                            0 & 0 & 0 & 0
                        \end{array}
                 \right)                 
\mathbf{G}_{5} &= \left(\begin{array}{cccc}
                            0 & 0 &-1 & 0 \\
                            0 & 0 & 0 & 0 \\
                            1 & 0 & 0 & 0 \\
                            0 & 0 & 0 & 0
                        \end{array}
                 \right)                 
\mathbf{G}_{6} = \left(\begin{array}{cccc}
                            0 & 1 & 0 & 0 \\
                           -1 & 0 & 0 & 0 \\
                            0 & 0 & 0 & 0 \\
                            0 & 0 & 0 & 0
                        \end{array}
                 \right).
\end{align*}

\noindent For the matrix logarithm and other Lie groups, please refer to \citep{Rossman2002}.


\chapter{Supplemental videos}
\label{appendix:supplemental_videos}

\begin{figure}[h!]
  \begin{minipage}[c]{0.37\textwidth}
    \includegraphics[width=\textwidth]{../materials/figs/appendix/video1.png}
  \end{minipage}\hfill
  \begin{minipage}[c]{0.6\textwidth}
    \raggedright
    \caption*{Phantom Simulation Sequence. \\ \\ \url{http://youtu.be/UyLnC6De1kw}}
  \end{minipage}
\end{figure}

\begin{figure}[h!]
  \begin{minipage}[c]{0.37\textwidth}
    \includegraphics[width=\textwidth]{../materials/figs/appendix/video2.png}
  \end{minipage}\hfill
  \begin{minipage}[c]{0.6\textwidth}
    \raggedright
    \caption*{Dense Monocular Camera Tracking \\ (CMIG 2013) \\ \\ \url{http://youtu.be/fPWp8XiurCI}}
  \end{minipage}
\end{figure}

\begin{figure}[h!]
  \begin{minipage}[c]{0.37\textwidth}
    \includegraphics[width=\textwidth]{../materials/figs/appendix/video3.png}
  \end{minipage}\hfill
  \begin{minipage}[c]{0.6\textwidth}
    \raggedright
    \caption*{Real-Time Dense Stereo Reconstruction for Laparoscopic Prostatectomy \\ (MICCAI 2013) \\ \\ \url{http://youtu.be/jqfsv-G7of0}}
  \end{minipage}
\end{figure}

\begin{figure}[h!]
  \begin{minipage}[c]{0.37\textwidth}
    \includegraphics[width=\textwidth]{../materials/figs/appendix/video4.png}
  \end{minipage}\hfill
  \begin{minipage}[c]{0.6\textwidth}
    \raggedright
    \caption*{Real-Time Dense Stereo Camera Tracking for Laparoscopic Prostatectomy \\ (IPCAI 2014) \\ \\ \url{http://youtu.be/pwaf3dd43W0}}
  \end{minipage}
\end{figure}