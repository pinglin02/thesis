\chapter{Introduction}
\minitoc
\newpage


\section{Introduction}
Minimally invasive surgery (MIS) is an increasingly popular treatment option due to reduced operative trauma compared to traditional open surgery, and can provide benefits of lower expense, shorter recovery, and reduced incidence of post-surgical complications. In order to perform an operation through small incisions in the skin, \gls{MIS} uses endoscopic devices to indirectly observe the surgical scene. Due to the nature of live endoscopic video, however, there are severe constraints on the surgeon's spatial perception and reduced operative visual information. Figure~\ref{fig:mis_types} shows a general scene in \gls{MIS}.

In the example of robot-assisted laparoscopic prostatectomy (RALP), although the da Vinci system provides a magnified 3D visualisation along with intuitive scaled manual interaction, the rates of complication from this procedure are still comparable to open surgery. Furthermore, a systematic review has shown current clinical RALP stringently requires more efforts in methodological standards for clinical research on new urologic procedures and devices~\citep{Kang2010}. In this scenario, an auxiliary system providing additional visual information could be advantageous and may improve the results of \gls{RALP} beyond that of open surgery. 

The first image-guided surgery can be sourced back to 1895 when a patient had surgery to remove an inserted needle in her hand by surgeons using an X-ray print to help locate the needle. This can be seen as the first surgical procedure guided by an image and happened just two months after X-rays were discovered~\citep{Webb1988}. 

Nowadays surgeons in the operation theatre can easily acquire various modalities to help with surgery. Besides X-rays, high quality preoperative 3D computerised tomography (CT) and magnetic resonance Imaging (MRI) models enable surgeons to analyse the patients' anatomy, physiology and pathology. These 3D models are, however, independent of the surgeons' viewing position. In a usual scenario, surgeons refer those preoperative models off-line as images on a separate screen or light box and perform the surgery using their mental impression of the patient's anatomy and pathology.

Real-time interactive guidance modalities including ultrasound, intravascular ultrasound (IVUS) or X-ray fluoroscopy provide instant visualisation of the underlying anatomy so that the surgical tools and the operative targets are both visible in the image and the intervention proceeds until the task is finished. However, these intraoperative imaging modalities often suffer from quality issues, such as poor resolution, 2D visualisation and low signal-to-noise ratio. In general the quality of intraoperative imaging is poor compared to that which can be obtained preoperatively. 

Different imaging modalities depict different anatomic structures, depending on their physical design principle. A desirable image-guided system would be able to integrate different modalities into a novel view which summarises information for the surgeons. Such an ideal system has to solve one big problem first - how to unify those modalities which have different coordinates. Although this is a classic image registration problem which has been studied for decades, it still remains a difficult challenge for image guidance which may have issues such as failed correspondence matching, deformation modelling and so on.

An alternative is to use a navigation system which can track the surgeon's viewing frame so it can augment virtual information or preoperative models into the right place. Once the virtual objects are locked in, the navigation system takes over the alignment task frame-by-frame. As a consequence, the augmented objects will stick to the correct pose in 3D no mater how surgeon's viewing angle changes.

\begin{figure}[t]
    \centering
    \hfill\null
    \subfloat[\label{fig:typical_mis}]{\includegraphics[width=0.5\textwidth]{../materials/figs/ch1/mis_laparoscope.png}}
    \hfill
    \subfloat[\label{fig:robot_mis}]{\includegraphics[width=0.5\textwidth]{../materials/figs/ch1/mis_davinci.png}}
    \hfill\null
    \caption[Typical MIS and robotic MIS.]{Typical \gls{MIS} (a) and robotic-assisted MIS (b).}
    \label{fig:mis_types}
    \hrulefill
\end{figure}

\section{Vision-based tracking for image-guided surgery}
In the early days, stereotactic frames were regarded as the most accurate tracking method for surgery guidance~\citep{Jensen1996}. In this method, a stereotactic frame is rigidly attached to the patient to provide a reference frame for targeting points as shown in Figure~\ref{fig:stereotactic_frame}. Specifically, the frame is screwed into the skull prior to brain imaging, which is generally performed immediately before the operation. Reference points on the frame are identified in these images, as is the target point within the lesion and entry path. A system consisting of two accurately gauged protractors allows the alignment of a biopsy needle along the desired approach and insertion to a predetermined depth. The frame installation is unavoidably an invasive procedure and a somewhat unpleasant experience for the patient. The size of the frame also creates problems for more open surgery and is infeasible for \gls{MIS}. Therefore such a fixed frame system is limited. A frameless system would clearly be preferable. 

A frameless option requires a tracking system to define a coordinate system within the operating theatre. The most popular tracking systems for \gls{IGS} use optical images. The basic idea is to find a point in the tracker's multiple 2D camera views, from which simple triangulation will give the 3D position of the point relative to the cameras. The localised points can be either active (infra-red emitting diodes, IREDs) or passive (highly reflecting spheres or high contrast points) as shown in Figure~\ref{fig:optical_tracker}. The main difficulty with optical tracking is that line-of-sight between the tracker's cameras and tracked objects must be maintained. An additional possible error source with the reflecting sphere system is partial occlusion of the sphere or contamination with blood, which may shift the measured location. In addition, the accuracy of tracking a target diminishes as you get further away from the markers due to rotational error~\cite{Fitzpatrick1998}. As a result, this method is not suitable for accurately tracking the endoscope or laparoscopic camera in \gls{MIS} since any external markers must be a long way from the end of the scope to be visible.

A particular goal of this thesis is to approach endoscopic/laparoscopic camera tracking in a different way. We aim to provide a relatively practical pure vision-based tracking system for image-guided surgery. As shown in Figure~\ref{fig:ego_tracking}, while the camera exposes the scene, it also locates itself on-the-fly. Such a tracking method is termed \textit{vision-based ego-motion estimation} or \textit{visual odometry} in the field of robotic vision. Furthermore, if the camera can track self-pose while maintaining an environment map for correcting drift, the method is known as \textit{visual simultaneous localisation and mapping} (SLAM). 

\begin{figure}[t]
    \centering
    \hfill\null
    \subfloat[\label{fig:stereotactic_frame}]{\includegraphics[width=0.28\textwidth]{../materials/figs/ch1/stereotactic_frame.png}}
    \hfill
    \subfloat[\label{fig:optical_tracker}]{\includegraphics[width=0.34\textwidth]{../materials/figs/ch1/optical_tracker.pdf}}
    \hfill
    \subfloat[\label{fig:ego_tracking}]{\includegraphics[width=0.3\textwidth]{../materials/figs/ch1/ego_tracking.pdf}}
    \hfill\null
    \caption[Different tracking methods for IGS.]{Different tracking methods for \gls{IGS}. (a) Stereotactic frame (Leksell). (b) Optical tracking system (Atracsys). (c) The preferable ego-motion tracking method.}
    \hrulefill
\end{figure}

\section{Why dense vision?}
Camera ego-motion estimation is a widely studied problem in the computer vision and robotic vision community. It turns out that the essence of a tracking problem is equivalent to image registration, in which consequent camera frames are aligned with a reference frame by solving some mathematical models (Section~\ref{sec:bayesian_inference_dense_vision}). Based on this principle, the algorithms for the registration task can be categorised into two main approaches - \textit{Dense methods} and \textit{feature-based methods}.


Feature-based methods obtain the registration transformation by feature matching as a front-end step. A feature point in an image is detected and represented by a feature descriptor. Given two images with their set of feature points, the matching task is to match the most similar features in the two images according to the feature descriptor. The transformation between the two images can then be found by solving an overdetermined linear system formed by the feature correspondences~\citep{Davison2003, Klein2007}.


As a counterpart, dense methods directly obtain the transformation from image intensities. A pixel-wise cost function is devised and optimised as the degree of similarity between all pixels in the two images, together with a certain motion parameterisation for warping either image in order to align the two. The optimisation starts from an appropriate initial estimate (within the convex basin) so that a gradient-descent-based algorithm can find the global extreme of the cost function~\citep{Comport2010, Newcombe2010, Newcombe2011a, Newcombe2011b}.

Which methodology is superior to the other is still an open debate. Feature-based methods globally match the descriptors so that they allow very wide baseline pose estimation such as in a large scale \textit{structure-from-motion} task. In contrast, dense methods, by their nature, are ill-suited for wide baseline image matching. However, while feature-based methods apparently require a texture abundant scene in order to collect enough robust correspondences, dense methods that exploit all pixel intensities for the optimisation are more suitable for tracking in a texture-poor scene. It is clear that we can at least conclude that the method with best performance really depends on the target application. Decent discussions and comparisons of the two methods in the literature can be found in~\citep{Irani2000, Triggs2000}. Handa also presented a systematic summary for the advantages of dense methods~\cite[Section 4.1]{Ankur2013}.

The state of the art feature-based method assumes good scene conditions such that the scene is static, there are a sufficient number of feature points to track and  the lighting is constant~\citep{Klein2007}. However in endoscope/laparoscope images, a texture-poor, deforming, smoky and specular scene is expected. When applying the feature-based method to such a scene, it is doomed to failure due to all the bad conditions that will appear in the endoscopic/laparoscopic scenes. In addition, the small baseline assumption can be perfectly met in a camera tracking application, where the frame-rate and motion speed are such that consecutive frames are very close to one another. Furthermore, using the da Vinci's stereoscopic camera enables us to reconstruct an instant 3D scene structure on-the-fly so there is no need for a pose/map initialisation step~\citep{Klein2007, Newcombe2011a}. We therefore have focused on dense methods for realising an endoscopic/laparoscopic tracking system. 

\section{Contributions}
This thesis aims to provide a robust camera ego-motion estimation system for augmented reality in \gls{IGS} particularly for \gls{RALP}. The original thought was to use the-state-of-the-art visual \gls{SLAM} system \gls{PTAM}, to bootstrap a number of camera poses for subsequent 3D to 2D preoperative models registration followed by dense tracking using the registered models. The preliminary research described in Chapter~\ref{ch:mapping_and_tracking_using_preoperative_models} has found that \gls{PTAM} cannot provide sufficiently accurate pose estimations and that discrepancies between the preoperative model and the scene cause registration failure and camera tracking drift. Nevertheless, in the initial studies we have also found that dense tracking is extremely robust by itself even in an endoscopic scene. It is therefore promising to track the endoscopic camera using dense methods.

Starting from Chapter~\ref{ch:real_time_dense_stereo_reconstruction}, we have redirected our approach to the problem. Instead of registering the preoperative models into the endoscopic scenes, a state-of-the-art stereo reconstruction algorithm using the stereoscopic camera equipped on the da Vinci robotic platform has been proposed. According to comprehensive validation studies, the proposed variational-based dense method, which uses \gls{ZNCC} as the data term and Huber norm as the regulariser term, has shown the best performance among other previous algorithms and imaging sensors.


In Chapter~\ref{ch:real_time_sense_stereo_camera_tracking}, based on the early work of \cite{Comport2010}, a stereoscopic dense visual odometry system is proposed. It exploits all the stereo image data for visual odometry, by using the quadrifocal geometry constraint together with the dense stereo reconstructed model. The dense methods have turned out to be extremely robust even within a terribly ill-conditioned scenes which involves instrument motion, tissue deformation, specular highlights, and smoke. Consequently the proposed system has overcome all the problems we met in Chapter~\ref{ch:mapping_and_tracking_using_preoperative_models} and constitutes a state-of-the-art camera tracking system in \gls{IGS}.

\section{Publications}
This thesis has led to the following publications:

\begin{enumerate}

\item \textbf{Ping-Lin Chang}, Dongbin Chen, and Philip ``Eddie'' Edwards. \textbf{Registration of a 3D preoperative model with 2D endoscopic images using parallel tracking and mapping (PTAM) with colour-consistency}. In Online Proceedings of the Medical Image Analysis and Understanding (MIUA), 2011. 

\item \textbf{Ping-Lin Chang}, Dongbin Chen, Daniel Cohen, and Philip ``Eddie'' Edwards. \textbf{2D/3D registration of a preoperative model with endoscopic video using colour-consistency}. In Proceedings of the Augmented Environments for Computer-Assisted Interventions (AE-CAI) in Conjunction with MICCAI, volume 7264, pages 1-12, 2012.

\item Qinquan Gao, \textbf{Ping-Lin Chang}, Daniel Rueckert, S Mohammed Ali, Daniel Cohen, Philip Pratt, Erik Mayer, Guang-Zhong Yang, Ara Darzi, Philip ``Eddie'' Edwards. \textbf{Modeling of the bony pelvis from MRI using a multi-atlas AE-SDM for registration and tracking in image-guided robotic prostatectomy}. Computerized Medical Imaging and Graphics (CMIG), 37(2):183-194, 2013.

\item \textbf{Ping-Lin Chang}, Danail Stoyanov, Andrew J. Davison, and Philip ``Eddie'' Edwards. \textbf{Real-time dense stereo reconstruction using convex optimisation with a cost-volume for image-guided robotic surgery}. In Proceedings of the Medical Image Computing and Computer-Assisted Intervention (MICCAI), volume 8149, pages 42-49, 2013.


\item L. Maier-Hein, A. Groch, A. Bartoli, S. Bodenstedt, G. Boissonnat, \textbf{P.-L. Chang}, N.T. Clancy, D. S. Elson, S. Haase, E. Heim, J. Hornegger, P. Jannin, H. Kenngott, T. Kilgus, B. M{\"u}ller-Stich, D. Oladokun, S. R{\"o}hl, T. R. {Dos Santos}, H.P. Schlemmer, A. Seitel, S. Speidel, M. Wagner and D. Stoyanov. \textbf{Comparative validation of single-shot optical techniques for laparoscopic 3D surface reconstruction}. To appear in IEEE Transactions on Medical Imaging (TMI), 2014.

\item \textbf{Ping-Lin Chang}, Ankur Handa, Andrew J. Davison, Danail Stoyanov, and Philip ``Eddie" Edwards. \textbf{Robust real-time visual odometry for stereo endoscopy using dense quadrifocal tracking}. In Proceedings of the Information Processing in Computer-Assisted Interventionas (IPCAI), pages 11-20. 2014.

\end{enumerate}

\section{Thesis structure}
Relevant background reviews will be introduced at the beginning of each chapter. This thesis is organised as follows

\begin{description}[leftmargin=!,labelwidth=\widthof{\bfseries Chapter X}]
\item[\small\textbf{Chapter}~\ref{ch:preliminaries}] introduces the materials and mathematical tools that will be used through out this thesis.
\item[\small\textbf{Chapter}~\ref{ch:mapping_and_tracking_using_preoperative_models}] describes our preliminary trail to approach the camera tracking problem in \gls{IGS}.
\item[\small\textbf{Chapter}~\ref{ch:real_time_dense_stereo_reconstruction}] provides a novel stereo reconstruction algorithm which outperforms the previous methods.
\item[\small\textbf{Chapter}~\ref{ch:real_time_sense_stereo_camera_tracking}] presents the overall dense visual odometry system that is the-state-of-the-art for endoscopic camera tracking in \gls{RALP}.
\item[\small\textbf{Chapter}~\ref{ch:conclusions_future}] concludes this thesis and gives some future directions for the possible extensions of this thesis.
\end{description}





